---
title: "Creating a Makefile"
weight: 20
---

The most challenging part of starting a C project is the build system.  The
good news is that it's easy, if somebody explains them to you.

## Create a Target

A target is something that's going to get built.  In our case, this is the life
program.  Write the name of the program at the left edge of the file, followed
by a colon.

    life:

## Identify Dependencies

Our life program is built from two files: main.c and life.c.  Before the final
program is build, main.c and life.c will be compiled to object code, which
lives in the files main.o and life.o.  The object files are the most immediate
dependencies of life.

The dependencies are listed on the same line as the target, after the colon.

    life:  life.o main.o rules.o

### What's Going On?

Make was designed for the purpose of bulding C programs, so it has default
rules which, when they see a dependency ending in .o, looks for a matching file
that ends in .c and tries to compile that according to certain rules.

## Assembling the Final Program

Finally, we need to tell make how to build life from the listed dependencies.
There are multiple ways, but the easiest is to just give the list of
dependencies to the C compiler, tell it what we want the output to be called,
and let it do the right thing.

    life: life.o main.o rules.o
            gcc life.o main.o rules.o -o life

IMPORTANT: On the second line of the recipe, it is very important that the line
starts with a tab character (Ascii 8), not a series of spaces (Ascii 32).  Make
is very sensitive to this, so you must use an editor which allows you to put in
tab characters.  VS Code and Atom are both aware of Makefiles and will do the
right thing.

## Simplifying the Recipe

Everything on the target line has been repeated on the line below.  This is
repetitive, and programmers don't like to repeat themselves.  Naturally,
there's a better way.

`$^` - A variable which will expand to include all of the dependencies from the line above (hence the up arrow).

`$@` - A variable which will expand to be the name of the target, which is "life" in this case.

`$(CC)` - Expands to the name of the compiler.  On most UNIX systems this will be gcc, but it's also possible to change your compiler.

With those two variables, the recipe is much easier to write.

    life: life.o main.o rules.o
            $(CC) $^ -o $@


# Building life

If you have saved your Makefile, you can now build life from the command line,
in the folder where you have saved Makefile, life.c and main.c

    $ make
    gcc    -c -o life.o life.c
    gcc    -c -o main.o main.c
    gcc    -c -o rules.o rules.c
    gcc life.o main.o rules.o -o life

The first two lines are the special default rule for creating files that end in
.o.  The final line is the recipe from our Makefile, with all of the special
variables expanded.

This should generate a file `life` in the `src` folder.  From inside the
container, run the program with

    ./life

It should exit with no output.  This isn't very exciting, but it means that you
have a working Makefile.
