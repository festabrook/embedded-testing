---
title: "Tools"
date: 2018-12-31T13:07:31-05:00
lastmod: 2018-12-31T13:07:31-05:00
tags:
- intro
categories: 
keywords:
weight: 10
---

## A Partner

The workshop will be significantly easier if you work with a pair partner, or as part of a small mob.  When you get stuck, another set of eyes can help you find a solution, or at least a path to explore.  You will choose one computer from the group to work on.  Generally it helps to pick the computer most close to a starndard install.  Using vi bindings for your terminal might make it easier for you to work, but it will confuse and frustrate everyone else.

## Tools You Will Need

1. [Git](https://git-scm.com/).  If you are a professional developer you probably already have this on your computer.
1. A code editor.  Pick whatever works the best for your group.  Personal preferences are [VS Code](https://code.visualstudio.com/) and [Atom](https://atom.io/).  Both have C/C++ plugins, which will make editing your code much easier.  Older editors, such as vi and emacs, should only be used if everyone in your group is familiar with them and comfortable working with them all day.  

## How to Run the Code

There are three ways to run the code for this workshop.

  1. [GitLab WebIDE](gitlabwebide)
  1. [Docker](using-docker)
  1. [Native Toolchain](native-toolchain)