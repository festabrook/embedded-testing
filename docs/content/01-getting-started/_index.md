---
title: "Getting Started"
weight: 10
date: 2019-04-27
---

## Agenda

1. Meet a pair partner.
1. Determine how you will build your code.
1. Set up your tools.