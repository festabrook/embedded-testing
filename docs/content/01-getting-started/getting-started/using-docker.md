---
title: "Using Docker"
weight: 25
---

If you don't want to run your builds as part of a GitLab CI/CD pipeline, it's perfectly acceptable to run your builds in a Docker image.  It will require you to do some setup on your machine.

  1. Install [Docker](https://www.docker.com/get-started)
  1. Build the image. In the folder where you checked out this repository:

    `docker build -t embeddedtestingdev`

  1. Create the container:

    `docker run  --name embeddedtesting  -v ${PWD}:/src -t -i embeddedtestingdev /bin/bash`

You should see a Linux command prompt now.

### If You Leave the Container

If you leave the container, or you shut down the machine, you may need to restart the container:

    docker start embeddedtesting

You can get back in to your container with:

    docker attach embeddedtesting

## To Edit Files

Open your editor in the folder where you checked out the repository, running in your host operating system, not the docker container just built.  It is strongly recommended that you use a code editor rather than an IDE.  IDEs do not deal well with the way we will be building files.

## To Build / Run Tests

    cd /src    # If you aren't already in the /src folder
    make test  # To run tests