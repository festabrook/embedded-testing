---
title: "Structuring Your Program for Testing"
date: 2019-04-27
weight: 50
---

## Agenda

1. Discuss how to change program structure to improve testing.
1. What can we learn from functional programming?
1. Implement a more functional solution?