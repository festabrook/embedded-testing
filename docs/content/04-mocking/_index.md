---
title: "Mocking and Test Doubles"
weight: 40
date: 2019-04-27
---

## Agenda

1. More Makefile hijinks (add support for mocks).
1. How to fake a header file.
1. Creating mock functions.
1. Integrate third party libraries.