#ifndef _ARDUINO_H_
#define _ARDUINO_H_

#include <stdint.h>

#define LED_BUILTIN 13
#define OUTPUT 0x1

void pinMode(uint8_t pinNumber, uint8_t direction);
void digitalWrite(uint8_t pinNumber, uint8_t value);
void delay(unsigned long milliseconds);

#endif